module.exports = {
  parser: 'babel-eslint',
  plugins: ['prettier'],
  env: { jest: true, browser: true, es6: true },
  extends: ['airbnb-base', 'prettier'],
  rules: {
    'import/prefer-default-export': 'off',
    'no-unused-vars': 'off',
    'no-prototype-builtins': 'off',
    'linebreak-style': 'off',

    'no-multi-assign': 'off',
    'func-names': 'off',
    'object-shorthand': 'off',
    'no-throw-literal': 'off',
    'no-extend-native': 'off',
    'prefer-template': 'off',
    'prefer-arrow-callback': 'off',
    'no-restricted-syntax': 'off',
    'prettier/prettier': ['error', { singleQuote: true }],
    quotes: ['error', 'single'],
    'arrow-parens': ['warn', 'as-needed'],
    'no-console': 'off',
    'no-shadow': 'off',

    'no-await-in-loop': 'off',
    'no-param-reassign': 'off',
    'import/no-extraneous-dependencies': [
      'error',
      { devDependencies: ['**/*.test.js'] },
    ],
  },
};
