# Simple Javascript Starter

Simple Javascript project for Javascript development using [eslint](http://eslint.org/) for linting, [eslint-config-airbnb-base](https://www.npmjs.com/package/eslint-config-airbnb-base) for default eslint configuration, [babel](https://babeljs.io) for ES6 transpilation, [webpack].(http://webpack.github.io/docs/) for module bundling, , [prettier](https://github.com/prettier/prettier) for formatting, [jest](https://facebook.github.io/jest/) for unit testing and [webpack-dev-server](https://webpack.github.io/docs/webpack-dev-server.html) as live reloading static http server. Supports auto completion in [visual studio code](https://code.visualstudio.com) using typescript definition files.

## Usage

1.  Install [nodejs](https://nodejs.org/en/). I highly recommend using [nvm](https://github.com/creationix/nvm). If you are on mac or ubuntu, you could use the following command to install nodejs. It's from my [dotfiles](https://gitlab.com/seartipy/dotfiles) configuration.

    On Mac

        curl -L j.mp/srtpdtf > setup && bash setup nodefaults web

    On Ubuntu

        wget -qO- j.mp/srtpdtf > setup && bash setup nodefaults web

You could also install and configure Emacs, or Visual studio code by adding option `emacs`, `vscode` respectively to above `web` and `nodefaults`. Drop `nodefaults` if you want an awesome prompt in zsh and bash. For examples to install web and visual studio code you could

        curl -L j.mp/srtpdtf > setup && bash setup nodefaults web vscode      # MacOS
        wget -qO- j.mp/srtpdtf > setup && bash setup nodefaults web vscode    # Ubuntu

2.  Clone this repository and install npm packages. Make sure you have [git](https://git-scm.com/) installed.

        git clone https://gitlab.com/pervezfunctor/simple-javascript-starter.git
        npm install -g yarn
        cd simple-javascript-starter
        yarn

3.  Start server

        yarn start

    Now you can edit `src/index.js` in any editor and see your changes in browser immediately.
    You could use `yarn test` to run jest tests.
    You could use `yarn run lint` for linting all files in `src` folder.

4.  Setup your editor

    If you use `visual studio code`, install `visual studio code extensions` using the following command( or use the script in Step 1).

        code --install-extension EditorConfig.EditorConfig
        code --install-extension dbaeumer.vscode-eslint
        code --install-extension msjsdiag.debugger-for-chrome
        code --install-extension Orta.vscode-jest
        code --install-extension esbenp.prettier-vscode


    On Mac, you might have to [install shell command](https://code.visualstudio.com/docs/setup/mac).

## License

MIT
