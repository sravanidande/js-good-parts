// @ts-nocheck
import '@babel/polyfill';

import { range } from 'rxjs';
import { map, filter, reduce } from 'rxjs/operators';

import { assert } from 'tcomb';

range(1, 10)
  .pipe(
    filter(x => x % 2 === 0),
    map(x => x * x),
    reduce((x, y) => x + y),
  )
  .subscribe(x => assert(x === 220));

console.log('helo');

const stooge = {
  'first-name': 'jerome',
  'last-name': 'howard',
};

const flight = {
  airLine: 'oceanic',
  number: 815,
  departure: {
    IATA: 'SYD',
    time: '2004-09-22 10:42',
    city: 'Los Angeles',
  },
};

// retriving an object

const name = stooge['first-name']; // "jerome"

const city = flight.departure.IATA; // "SYD"

const midName = stooge['middle-name']; // Undefined

const infor = flight.status; // undefined

const name2 = stooge['FIRST-NAME']; // undefined

// The || operator can be used to fill default values

const middle = stooge['middle-name'] || '(none)';

const status = flight.status || 'unknown';

const equip = flight.equipment; // undefined

const modelInfo = flight.equipment.model; // throw "TypeError"

const info = flight.equipment && flight.equipment.model; // undefined

// UPDATING

stooge['first-name'] = 'jerome';

stooge['middle-name'] = 'Lester';

stooge.nickname = 'Curly';

flight.equipment = {
  model: 'Boeing 777',
};

flight.status = 'Overdue';

// Reference

const x = stooge;
x.nickname = 'Curly';
const nick = stooge.nickname; // "Curly"

let a = {};
let b = {};
let c = {};

a = b = c = {};

// Prototype

if (typeof Object.create !== 'function') {
  Object.create = function(o) {
    const F = function() {};
    F.prototype = o;
    return new F();
  };
}

const anotherStooge = Object.create(stooge);

anotherStooge['first-name'] = 'Harry';
anotherStooge['middle-name'] = 'Moses';
anotherStooge.nickname = 'Moe';

stooge.profession = 'actor';
const prof = anotherStooge.profession; // "actor"

// Reflection

console.log(typeof flight.number); // "number"
console.log(typeof flight.status); // "string"
console.log(typeof flight.arrival); // "object"
console.log(typeof flight.manifest); // "undefined"

console.log(typeof flight.toString); // "function"
console.log(typeof flight.constructor); // "function"

console.log(flight.hasOwnProperty('number')); // true
console.log(flight.hasOwnProperty('constructor')); // false

// Enumeration

for (const name in anotherStooge) {
  if (typeof anotherStooge[name] !== 'function') {
    console.log(`${name} + ':' + ${anotherStooge[name]}`);
  }
}

const properties = ['first-name', 'middle-name', 'last-name', 'profession'];
for (let i = 0; i < properties.length; i += 1) {
  console.log(`${properties[i]}+':'+${anotherStooge[properties[i]]}`);
}

// Deletion

const nickName = anotherStooge.nickname; // "Moe"

delete anotherStooge.nickname;

console.log(anotherStooge.nickname); // "Curly"

// Global Abatement

const MyApp = {};

MyApp.stooge = {
  'first-name': 'Joe',
  'last-name': 'howrd',
};

MyApp.flight = {
  airLine: 'oceanic',
  number: 815,
  departure: {
    IATA: 'SYD',
    time: '2004-09-22 10:42',
    city: 'Sydney',
  },
  arrival: {
    IATA: 'LAX',
    time: '2004-09-22 10:42',
    city: 'Los Angeles',
  },
};

// Function Literal

export const add = (a, b) => {
  console.log(a + b);
};

const sum = add(2, 3);
console.log(sum); // 5

const myObject = {
  value: 0,
  increment: function(inc) {
    this.value += typeof inc === 'number' ? inc : 1;
  },
};

myObject.increment();
console.log(myObject.value); // 1

myObject.increment(2);
console.log(myObject.value); // 3

// function invocation

myObject.double = function() {
  const that = this;

  const helper = function() {
    return add(that.value, that.value);
  };
  helper();
};

myObject.double();
console.log(myObject.getvalue()); // 6

// constructor invocation

const Quo = function(string) {
  this.status = string;
};

Quo.prototype.get_status = function() {
  return this.status;
};

const myQuo = new Quo('confused');

console.log(myQuo.get_status()); // confused

// Apply Invocation pattern

const array = [3, 4];
const total = add(...array); // sum is 7

const addition = array => {
  let sum = 0;
  for (let i = 0; i < array.length; i += 1) {
    sum += array[i];
  }
  return sum;
};

console.log(addition([4, 8, 15, 16, 23, 42])); // 108

// Exceptions

const add1 = function(a, b) {
  if (typeof a !== 'number' || typeof b !== 'number') {
    throw {
      name: 'TypeError',
      message: 'add needs numbers',
    };
  }
  return a + b;
};

const tryIt = function() {
  try {
    add('seven');
  } catch (e) {
    console.log(e.name + ':' + e.message);
  }
};

tryIt();

Function.prototype.mehod = function(name, func) {
  this.prototype[name] = func;
  return this;
};

Number.method('integer', function() {
  return Math[this < 0 ? 'ceiling' : 'floor'](this);
});

console.log((-10 / 3).integer()); // -3

String.method('trim', function() {
  return this.replace(/^\s+\s+$/g, '');
});

// console.log(${'""'}+" neat ".trim()+${'""'});

Function.prototype.mehod = function(name, func) {
  if (!this.prototype[name]) {
    this.prototype[name] = func;
  }
};

// recursion

const hanoi = function(disc, src, aux, dst) {
  if (disc > 0) {
    hanoi(disc - 1, src, dst, aux);
    console.log('Move disc' + disc + ' from ' + src + ' to ' + dst);
    hanoi(disc - 1, aux, src, dst);
  }
};

hanoi(3, 'Src', 'Aux', 'Dst');

const walkTheDom = function walk(node, func) {
  func(node);
  node = node.firstChild;
  while (node) {
    walk(node, func);
    node = node.nextSibling;
  }
};

const getElementByAttribute = function(att, value) {
  const results = [];
  walkTheDom(document.body, function(node) {
    const actual = node.nodeType === 1 && node.getAttribute(att);
    if (
      typeof actual === 'string' &&
      (actual === value || typeof value !== 'string')
    ) {
      results.push(node);
    }
  });
  return results;
};

const factorial = function factorial(i, a) {
  a = a || 1;
  if (i < 2) {
    return a;
  }
  return factorial(i - 1, a * i);
};

console.log(factorial(4));

const foo = function() {
  let a = 3;
  const b = 5;

  const bar = function() {
    let b = 7;
    const c = 11;
    a += b += c;
  };
  bar();
};

const myObj = (function() {
  let value = 0;
  return {
    increment: function(inc) {
      value += typeof inc === 'number' ? inc : 1;
    },
    getvalue: function() {
      return value;
    },
  };
})();

const quo = function(status) {
  return {
    get_status: function() {
      return status;
    },
  };
};

const myQue = quo('amazed');

console.log(myQue.get_status());

const fade = function(node) {
  let level = 1;
  const step = function() {
    const hex = level.toString(16);
    node.style.backgroundColor = '#FFFF' + hex + hex;
    if (level < 15) {
      level += 1;
      setTimeout(step, 100);
    }
  };
  setTimeout(step, 100);
};

fade(document.body);

const addTheHandlers = function(nodes) {
  for (let i = 0; i < nodes.length; i += 1) {
    nodes[i].onClick = (function(i) {
      return function(e) {
        console.log(e);
      };
    })(i);
  }
};

function prepareTheRequest() {}

function sendRequestSynchronously() {}

const request = prepareTheRequest();
const response = sendRequestSynchronously(request);
console.log(response);

const req = prepareTheRequest();

sendRequestSynchronously(req, function(response) {
  console.log(response);
});

String.method(
  'deentityify',
  (function() {
    const entity = {
      quot: '"',
      lt: '<',
      gt: '>',
    };

    return function() {
      return this.replace(/&([^&;]+);/g, function(a, b) {
        const r = entity[b];
        return typeof r === 'string' ? r : a;
      });
    };
  })(),
);

const serialMaker = function() {
  let prefix = '';
  let seq = 0;
  return {
    set_prefix: function(p) {
      prefix = String(p);
    },
    set_seq: function(s) {
      seq = s;
    },
    gensym: function() {
      const result = prefix + seq;
      seq += 1;
      return result;
    },
  };
};

const seqer = serialMaker();
seqer.set_prefix = '0';
seqer.set_seq = 1000;
const unique = seqer.gensym();

function getElement() {}

getElement('myBoxDiv')
  .move(350, 150)
  .width(100)
  .height(100)
  .color('red')
  .border('10px outset')
  .padding('4px')
  .appendText('Please stand by')
  .on('mousedown', function(m) {
    this.startDrag(m, this.getNinth(m));
  })
  .on('mousemove', 'drag')
  .on('mouseup', 'stopDrag')
  .later(2000, function() {
    this.color('yellow')
      .setHTML('What hath God wraught?')
      .slide(400, 40, 200, 200);
  })
  .tip('This box is resizeable');

const add2 = add.curry(1);
console.log(add2(6));

// Function.method('curry', function() {
//   const args = arguments,
//     that = this;
//   return function() {
//     return that.apply(...arguments);
//   };
// });
