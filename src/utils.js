export const merge = obj1 => obj2 => ({ ...obj1, ...obj2 });

export const update = (k, f) => obj => ({
  ...obj,
  [k]: f(obj[k]),
});

export const omitOne = key => obj => {
  const { [key]: _, ...rest } = obj;
  return rest;
};

export const slice = (start, stop) => arr =>
  stop !== undefined ? arr.slice(start, stop) : arr.slice(start);

export const push = (...values) => arr => [...arr, ...values];

export const unshift = (...values) => arr => [...values, ...arr];

export const pop = arr => slice(0, arr.length - 1)(arr);

export const shift = arr => {
  const [, ...rest] = arr;
  return rest;
};

export const insert = (index, ...values) => arr => [
  ...arr.slice(0, index),
  ...values,
  ...arr.slice(index),
];

export const remove = index => arr => [
  ...arr.slice(0, index),
  ...arr.slice(index + 1),
];

export const range = (start, stop) => {
  const result = [];
  const begin = stop !== undefined ? start : 0;
  const end = stop !== undefined ? stop : start;
  // eslint-disable-next-line
  for (let i = begin; i < end; i += 1) {
    result.push(i);
  }
  return result;
};

export const map = f => arr => arr.map(f);

export const filter = predicate => arr => arr.filter(predicate);

export const every = predicate => arr => arr.every(predicate);

export const some = predicate => arr => arr.some(predicate);

export const reduce = (f, seed) => arr => arr.reduce(f, seed);

export const reduceRight = (f, seed) => arr => arr.reduceRight(f, seed);

export const find = predicate => arr => arr.find(predicate);

export const findIndex = predicate => arr => arr.findIndex(predicate);

export const forEach = f => arr => arr.forEach(f);

export const indexOf = (searchElement, index) => arr =>
  arr.indexOf(searchElement, index);

export const lastIndexOf = (searchElement, index) => arr =>
  arr.lastIndexOf(searchElement, index);

export const zip = (...arrs) => {
  const min = Math.min(...arrs.map(arr => arr.length));
  return range(min).map(i => arrs.map(arr => arr[i]));
};

export const takeWhile = predicate => arr => {
  const result = [];
  // eslint-disable-next-line
  for (const [i, e] of arr.entries()) {
    if (!predicate(e, i)) {
      break;
    }
    result.push(e);
  }
  return result;
};

export const dropWhile = predicate => arr => {
  // eslint-disable-next-line
  for (const [i, e] of arr.entries()) {
    if (!predicate(e, i)) {
      return slice(i)(arr);
    }
  }
  return [];
};

export const take = n => arr => slice(0, n)(arr);

export const drop = n => arr => slice(n)(arr);

export const keys = obj => Object.keys(obj);

export const values = obj => Object.values(obj);

export const entries = obj => Object.entries(obj);

export const pipe = (obj, first, ...rest) =>
  reduce((result, fn) => fn(result), first(obj))(rest);

export const pick = (...ks) => obj => {
  const assign = (result, k) => {
    // eslint-disable-next-line
    result[k] = obj[k];
    return result;
  };
  return reduce(assign, {})(ks);
};

export const pluck = (...ks) => arr => arr.map(obj => pick(...ks)(obj));

export const omit = (...ks) => obj => {
  const omitter = (result, k) => {
    if (obj.hasOwnProperty(k) && ks.indexOf(k) === -1) {
      // eslint-disable-next-line
      result[k] = obj[k];
    }
    return result;
  };
  return reduce(omitter, {})(keys(obj));
};

export const join = seperator => arr => arr.join(seperator);

export const reverse = arr => {
  const result = slice(0)(arr);
  result.reverse();
  return result;
};

export const sortBy = cmp => arr => {
  const result = slice(0)(arr);
  result.sort(cmp);
  return result;
};

export const concat2 = arr => arr2 => arr.concat(arr2);

export const concat = (...arr) =>
  pipe(
    arr,
    reduce(
      (a1, a2) =>
        pipe(
          a2,
          concat2(a1),
        ),
      [],
    ),
  );

export const flatMap = f => arr => concat(...arr.map(f));

export const set = (key, value) => obj =>
  Array.isArray(obj)
    ? [...obj.slice(0, +key), value, ...obj.slice(+key + 1)]
    : { ...obj, [key]: value };

export const get = key => obj => (Array.isArray(obj) ? obj[+key] : obj[key]);

export const setIn = (value, ...ks) => obj => {
  if (ks.length === 0) {
    return obj;
  }
  const [first, ...rest] = ks;
  if (rest.length === 0) {
    return set(first, value)(obj);
  }
  return set(first, setIn(value, ...rest)(get(first)(obj)))(obj);
};

export const getIn = (first, ...rest) => obj => {
  // eslint-disable-next-line immutable/no-let
  let result = obj[first];
  // eslint-disable-next-line fp/no-loops
  for (const key of rest) {
    result = result[key];
  }
  return result;
};
