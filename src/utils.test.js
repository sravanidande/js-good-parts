import {
  pipe,
  map,
  pick,
  zip,
  take,
  drop,
  takeWhile,
  dropWhile,
  range,
  pluck,
  merge,
  get,
  set,
  update,
  omit,
  omitOne,
  concat,
  flatMap,
  push,
  unshift,
  pop,
  shift,
  insert,
  remove,
  filter,
  reduce,
  getIn,
  setIn,
} from './utils';

test('pick', () => {
  expect(pick('x', 'y')({ x: 1, y: 2, z: 3 })).toEqual({ x: 1, y: 2 });
  expect(pick('x')({ x: 1, y: 2 })).toEqual({ x: 1 });
  expect(pick('y')({ x: 1, y: 2 })).toEqual({ y: 2 });
  expect(pick('z')({ x: 1, y: 2 })).toEqual({});
});

test('pluck', () => {
  expect(
    pluck('x', 'y')([
      { x: 1, y: 2, z: 3 },
      { x: 11, y: 12, z: 13 },
      { x: 21, y: 22, z: 13 },
      { x: 31, y: 32, z: 33 },
    ]),
  ).toEqual([
    { x: 1, y: 2 },
    { x: 11, y: 12 },
    { x: 21, y: 22 },
    { x: 31, y: 32 },
  ]);
});

test('merge', () => {
  expect(merge({})({})).toEqual({});
  expect(merge({ x: 1 })({})).toEqual({ x: 1 });
  expect(merge({})({ y: 2 })).toEqual({ y: 2 });
  expect(merge({ x: 1 })({ y: 2 })).toEqual({ x: 1, y: 2 });
  expect(merge({ x: 1, y: 2 })({ z: 3 })).toEqual({ x: 1, y: 2, z: 3 });
  expect(merge({ z: 3 })({ x: 1, y: 2 })).toEqual({ x: 1, y: 2, z: 3 });
});

test('get', () => {
  expect(get('x')({ x: 1, y: 2 })).toEqual(1);
  expect(get('y')({ x: 1, y: 2 })).toEqual(2);
  expect(get('z')({ x: 1, y: 2 })).toBeUndefined();
  expect(get('x')({})).toBeUndefined();

  expect(get(0)([1])).toEqual(1);
  expect(get(0)([1, 2])).toEqual(1);
  expect(get(1)([1, 2])).toEqual(2);
  expect(get(0)([1, 2, 3])).toEqual(1);
  expect(get(1)([1, 2, 3])).toEqual(2);
  expect(get(2)([1, 2, 3])).toEqual(3);
});

test('set', () => {
  expect(set('x', 100)({})).toEqual({ x: 100 });
  expect(set('x', 100)({ x: 1, y: 2 })).toEqual({ x: 100, y: 2 });
  expect(set('y', 100)({ x: 1, y: 2 })).toEqual({ x: 1, y: 100 });
  expect(set('z', 100)({ x: 1, y: 2 })).toEqual({ x: 1, y: 2, z: 100 });

  expect(set(0, 10)([1])).toEqual([10]);
  expect(set(0, 10)([1, 2])).toEqual([10, 2]);
  expect(set(1, 10)([1, 2])).toEqual([1, 10]);
  expect(set(0, 10)([1, 2, 3])).toEqual([10, 2, 3]);
  expect(set(1, 10)([1, 2, 3])).toEqual([1, 10, 3]);
  expect(set(2, 10)([1, 2, 3])).toEqual([1, 2, 10]);
});

test('update', () => {
  const inc = x => x + 1;
  expect(update('x', inc)({ x: 1, y: 2 })).toEqual({ x: 2, y: 2 });
  expect(update('y', inc)({ x: 1, y: 2 })).toEqual({ x: 1, y: 3 });
});

test('omit', () => {
  expect(omit('x', 'y')({ x: 1, y: 2, z: 3 })).toEqual({ z: 3 });
  expect(omit('x')({ x: 1, y: 2 })).toEqual({ y: 2 });
  expect(omit('y')({ x: 1, y: 2 })).toEqual({ x: 1 });
  expect(omit('z')({ x: 1, y: 2 })).toEqual({ x: 1, y: 2 });
});

test('omitOne', () => {
  expect(omitOne('x')({ x: 1, y: 2, z: 3 })).toEqual({ y: 2, z: 3 });
  expect(omitOne('x')({ x: 1, y: 2 })).toEqual({ y: 2 });
  expect(omitOne('y')({ x: 1, y: 2 })).toEqual({ x: 1 });
  expect(omitOne('z')({ x: 1, y: 2 })).toEqual({ x: 1, y: 2 });
});

test('push', () => {
  expect(push(10)([])).toEqual([10]);
  expect(push(10)([1])).toEqual([1, 10]);
  expect(push(10)([1, 2])).toEqual([1, 2, 10]);
  expect(push(10)([1, 2, 3])).toEqual([1, 2, 3, 10]);
  expect(push(10, 20)([1, 2, 3])).toEqual([1, 2, 3, 10, 20]);
});

test('pop', () => {
  expect(pop([1])).toEqual([]);
  expect(pop([1, 2])).toEqual([1]);
  expect(pop([1, 2, 3])).toEqual([1, 2]);
});

test('unshift', () => {
  expect(unshift(10)([])).toEqual([10]);
  expect(unshift(10)([1])).toEqual([10, 1]);
  expect(unshift(10)([1, 2])).toEqual([10, 1, 2]);
  expect(unshift(10, 20)([1, 2])).toEqual([10, 20, 1, 2]);
});

test('shift', () => {
  expect(shift([1])).toEqual([]);
  expect(shift([1, 2])).toEqual([2]);
  expect(shift([1, 2, 3])).toEqual([2, 3]);
});

test('insert', () => {
  expect(insert(0, 10)([1])).toEqual([10, 1]);
  expect(insert(0, 10)([1, 2])).toEqual([10, 1, 2]);
  expect(insert(1, 10)([1, 2])).toEqual([1, 10, 2]);
  expect(insert(0, 10)([1, 2, 3])).toEqual([10, 1, 2, 3]);
  expect(insert(1, 10)([1, 2, 3])).toEqual([1, 10, 2, 3]);
  expect(insert(2, 10)([1, 2, 3])).toEqual([1, 2, 10, 3]);
  expect(insert(2, 10, 20)([1, 2, 3])).toEqual([1, 2, 10, 20, 3]);
});

test('remove', () => {
  expect(remove(0)([1])).toEqual([]);
  expect(remove(0)([1, 2])).toEqual([2]);
  expect(remove(1)([1, 2])).toEqual([1]);
  expect(remove(0)([1, 2, 3])).toEqual([2, 3]);
  expect(remove(1)([1, 2, 3])).toEqual([1, 3]);
  expect(remove(2)([1, 2, 3])).toEqual([1, 2]);
});

test('range', () => {
  expect(range(0)).toEqual([]);
  expect(range(10, 10)).toEqual([]);
  expect(range(100, 10)).toEqual([]);

  expect(range(0, 1)).toEqual([0]);
  expect(range(1, 5)).toEqual([1, 2, 3, 4]);

  expect(range(5)).toEqual([0, 1, 2, 3, 4]);
});

test('take', () => {
  expect(take(5)([])).toEqual([]);
  expect(take(0)([])).toEqual([]);
  expect(take(0)([])).toEqual([]);
  expect(take(1)([3])).toEqual([3]);
  expect(take(1)([3, 4])).toEqual([3]);
  expect(take(2)([3, 4])).toEqual([3, 4]);
  expect(take(5)([3, 4, 5, 6, 7])).toEqual([3, 4, 5, 6, 7]);
});

test('drop', () => {
  expect(drop(2)([])).toEqual([]);
  expect(drop(2)([1, 2, 3, 4])).toEqual([3, 4]);
  expect(drop(2)([1, 2])).toEqual([]);
  expect(drop(2)([1])).toEqual([]);
});

const isEven = x => x % 2 === 0;

test('takeWhile', () => {
  expect(takeWhile(isEven)([])).toEqual([]);
  expect(takeWhile(isEven)([3])).toEqual([]);
  expect(takeWhile(isEven)([5, 6])).toEqual([]);
  expect(takeWhile(isEven)([4])).toEqual([4]);
  expect(takeWhile(isEven)([4, 5])).toEqual([4]);
  expect(takeWhile(isEven)([4, 6, 8, 10])).toEqual([4, 6, 8, 10]);
});

test('dropWhile', () => {
  expect(dropWhile(isEven)([])).toEqual([]);
  expect(dropWhile(isEven)([1])).toEqual([1]);
  expect(dropWhile(isEven)([2])).toEqual([]);
  expect(dropWhile(isEven)([2, 3])).toEqual([3]);
  expect(dropWhile(isEven)([2, 4])).toEqual([]);
  expect(dropWhile(isEven)([1, 3])).toEqual([1, 3]);
  expect(dropWhile(isEven)([1, 2, 4, 5])).toEqual([1, 2, 4, 5]);
  expect(dropWhile(isEven)([2, 4, 6, 7, 8, 9])).toEqual([7, 8, 9]);
});

test('zip', () => {
  expect(zip([], [])).toEqual([]);
  expect(zip([1], [2])).toEqual([[1, 2]]);
  expect(zip([1, 2], [3, 4])).toEqual([[1, 3], [2, 4]]);
  expect(zip([1, 2, 3], [4, 5, 6])).toEqual([[1, 4], [2, 5], [3, 6]]);
  expect(zip([1, 2], [4, 5, 6])).toEqual([[1, 4], [2, 5]]);
  expect(zip([1, 2, 3], [4, 5, 6], [7, 8, 9])).toEqual([
    [1, 4, 7],
    [2, 5, 8],
    [3, 6, 9],
  ]);
});

test('pipe', () => {
  expect(
    pipe(
      [1, 2, 3, 4, 5, 6],
      filter(x => x % 2 === 0),
      map(x => x * x),
      reduce((x, y) => x + y, 0),
    ),
  ).toEqual(56);
});

test('concat', () => {
  expect(concat([], [], [])).toEqual([]);
  expect(concat([], [1], [])).toEqual([1]);
  expect(concat([1], [], [])).toEqual([1]);
  expect(concat([], [], [1])).toEqual([1]);
  expect(concat([1], [2], [3])).toEqual([1, 2, 3]);
  expect(concat([1, 2], [3, 4, 5], [6, 7, 8, 9])).toEqual([
    1,
    2,
    3,
    4,
    5,
    6,
    7,
    8,
    9,
  ]);
});

const incRepeatTwice = x => [x + 1, x + 1];

test('flatMap', () => {
  expect(
    pipe(
      [],
      flatMap(incRepeatTwice),
    ),
  ).toEqual([]);
  expect(
    pipe(
      [1],
      flatMap(incRepeatTwice),
    ),
  ).toEqual([2, 2]);
  expect(
    pipe(
      [1, 2],
      flatMap(incRepeatTwice),
    ),
  ).toEqual([2, 2, 3, 3]);
  expect(
    pipe(
      [1, 2, 3],
      flatMap(incRepeatTwice),
    ),
  ).toEqual([2, 2, 3, 3, 4, 4]);
});

test('getIn', () => {
  expect(
    pipe(
      { a: 1 },
      getIn('a'),
    ),
  ).toEqual(1);
  expect(
    pipe(
      { a: { b: 1 } },
      getIn('a', 'b'),
    ),
  ).toEqual(1);
  expect(
    pipe(
      { a: { b: { c: 1 } } },
      getIn('a', 'b', 'c'),
    ),
  ).toEqual(1);
  expect(
    pipe(
      { a: { b: { c: { d: 1 } } } },
      getIn('a', 'b', 'c', 'd'),
    ),
  ).toEqual(1);
  expect(
    pipe(
      { a: { b: [10, 20, { c: { d: [1, 2] } }] } },
      getIn('a', 'b', 2, 'c', 'd', 1),
    ),
  ).toEqual(2);
  expect(
    pipe(
      { a: { b: { c: { d: { e: 1 } } } } },
      getIn('a', 'b', 'c', 'd', 'e'),
    ),
  ).toEqual(1);
  expect(
    pipe(
      { a: { b: { c: 1 } } },
      getIn('a', 'b'),
    ),
  ).toEqual({ c: 1 });
});

test('setIn', () => {
  expect(
    pipe(
      { a: 1 },
      setIn(10, 'a'),
    ),
  ).toEqual({ a: 10 });
  expect(
    pipe(
      { a: { b: 1 } },
      setIn(10, 'a', 'b'),
    ),
  ).toEqual({ a: { b: 10 } });
  expect(
    pipe(
      { a: { b: { c: 1 } } },
      setIn(10, 'a', 'b', 'c'),
    ),
  ).toEqual({
    a: { b: { c: 10 } },
  });
  expect(
    pipe(
      { a: { b: { c: { d: 1 } } } },
      setIn(10, 'a', 'b', 'c', 'd'),
    ),
  ).toEqual({ a: { b: { c: { d: 10 } } } });
  expect(
    pipe(
      { a: { b: { c: { d: { e: 1, f: 20 } } } } },
      setIn(10, 'a', 'b', 'c', 'd', 'e'),
    ),
  ).toEqual({ a: { b: { c: { d: { e: 10, f: 20 } } } } });
  expect(
    pipe(
      { a: { b: [10, 20, { c: { d: [1, { f: 20 }] } }] } },
      setIn(10, 'a', 'b', 2, 'c', 'd', '1', 'f'),
    ),
  ).toEqual({ a: { b: [10, 20, { c: { d: [1, { f: 10 }] } }] } });
  expect(
    pipe(
      { a: { b: { c: 1, d: 20 } } },
      setIn(10, 'a', 'b'),
    ),
  ).toEqual({
    a: { b: 10 },
  });
});
